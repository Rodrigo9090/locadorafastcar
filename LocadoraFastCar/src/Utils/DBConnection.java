package Utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author RODRIGO OTAVIO
 */
public class DBConnection {
    private static EntityManager entityManager;
    
    private DBConnection() {}
    
    public static EntityManager getEntityManager() {
        
        if(entityManager == null) {
            EntityManagerFactory entityManagerFactory = 
                    Persistence.createEntityManagerFactory("LocadoraVeiculosPU");
            
            entityManager = entityManagerFactory.createEntityManager();
        }
        
        return entityManager;
    }
    
}
